# Homepage Swap Module
Homepage Swap is a Drupal module that empowers content editors to effortlessly switch between different content types without the hassle of publishing and unpublishing nodes individually.

## Features

- **Quick Switching:** Seamlessly switch between content types without the need to publish and unpublish nodes individually.
- **Selective Switching:** Choose specific content types that can be effortlessly switched between.
- **Permission Control:** Grant special permissions to active content editors for utilizing the switch functionality.
- **Intuitive Interface:** User-friendly interface for effortless content type switching.

## Installation

1. Download the Homepage Swap module from [Drupal.org](https://www.drupal.org/project/homepage_swap).
2. Extract the module files into the `modules` directory of your Drupal installation.
3. Enable the Homepage Swap module via the Drupal administration interface (`/admin/modules`).

## Configuration

1. Go to the permissions page in Drupal administration (`/admin/people/permissions`).
2. Grant the "Switch Content Types" permission to active content editors.
3. Configure the module settings to select which content types can be switched between.

## Usage

1. Navigate to the content editing page in Drupal.
2. Find the "Switch Content Type" option on the editing interface.
3. Select the desired content type from the dropdown menu.
4. Click "Switch" to swiftly change the content type without unpublishing and republishing nodes.


## Post-Installation

After installation, follow these additional steps to tailor Homepage Swap to your specific needs:

1. Visit `admin/config/homepage_swap/settings` and select which content types should be able to become the homepage.

2. Visit `admin/content/swap_homepage` to select which page is the active homepage.

## Contribution

Contributions are welcome! Fork the repository, make your changes, and submit a pull request. Please follow the [contribution guidelines](CONTRIBUTING.md).

## MAINTAINER

[Rob Loach] (https://www.drupal.org/u/robloach)